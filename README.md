To run a jekyll server, just type:

```
:::text
jekyll serve
```

And it will rebuild the pages as you edit them. You can see the pages at
http://localhost:8003
