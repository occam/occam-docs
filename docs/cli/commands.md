---
layout: doc_page
title: OCCAM Commands
---

# OCCAM Commands

All commands can append a `--base64` flag which means its arguments are encoded in base64. All commands also have a `--verbose`/`-v` flag which will output mostly diagnostic information about intermediate steps.

```
--base64      : arguments are encoded in base64
--verbose, -v : show 'noisy' level log statements
```

## attach

This command attaches an object to an experiment workflow.

## build

This command builds an object.

```
--to,       -t : The object ID to build
--revision, -r : The revision to build
--local,    -l : Build a local copy
```

### Building an Object

Within a directory containing an occam object file, you can simply type:

```
occam build
```

### Building a Specific Object

To see the build of a particular object, specify the object with the `--to`
and `--revision` options.

```
occam build --to 2223413a-92c6-11e4-b64f-001fd05bb228
```

For instance, this will build a very specific version of the object from a
particular point in time:

```
occam build --to 2223413a-92c6-11e4-b64f-001fd05bb228 --revision 6e7b377796d22cdbd0b6a4b103ca5ebb3458002a
```

### Developing an Object

To do incremental builds to test and develop an object against OCCAM, use the
`--local` flag:

```
occam build --local
```

The first time this command is used, a VM will be made for the directory you
are in. It will build the object inside this VM as usual by looking at the
[object metadata](/specifications/object.html) specifically the `build` section.

The second time the command is issued, the same VM will be used. Since the
current directory is mounted as the object instead of a separate space, any
changes you have made to the code within will be immediately visible. It will
simply invoke the build command again.

Generally, your build command should be something like a makefile that can
handle being invoked multiple times or handle partial builds when errors occur.

{% notice %}
When you use a local build, you cannot build experiments using that version of
the object. Instead, OCCAM ensures it uses a version built off of a clean,
known state. That is, it will rebuild your object entirely from scratch if you
use it within a workflow.
{% endnotice %}

Once you have something working in your local build, be sure to do a normal
`occam build` to ensure that it can be built from a clean start and preserve
the reproducibility.

## clone

This command clones the given object. It will take the object and copy it
to a new directory. It will then reassign it a new UUID, but it shall retain
its original history and [metadata](/specifications/object.html).
The original object will be tagged within the cloned object to ensure proper
*provenance* is available under the `clonedFrom` attribute.

You may now modify the object as much as you wish. You can `commit` the object
and use it within experiments. Nothing you do will interfere with the original
object or its authors.

Here is an example of how you would clone an application, in this case the
memory simulator [DRAMSim2](https://occam.cs.pitt.edu/objects/2223413a-92c6-11e4-b64f-001fd05bb228), and modify it:

{% highlight bash %}
occam clone --to 2223413a-92c6-11e4-b64f-001fd05bb228
cd simulator-DRAMSim2

# The initial build of your new version

occam build --local

# ... edit files ...

occam build --local

# repeat editing of files and building until satisfied

occam commit

# you may use your new version in experiments
{% endhighlight %}

In the above example, we used the `build` command to handle our development
within OCCAM. We can update the code and run incremental builds by using the
`build` command's `--local` option. We can do this as many times as we might
need to in order to produce a working modification to this application.

Then we `commit` the result and try it out in a normal experiment workflow.
We can always revisit and further modify the object and perform another
`commit` at any point.

## commit

This command updates the object store and record for this object to point at the current revision.

## configure

This command allows one to change configuration of objects in the current workflow.

## console

This command will yield a isolated shell that contains the object.

## DELETE

This command deletes all record of the given object from the current occam node.

## detach

This command will remove the reference to an object from an experiment workflow.

## initialize

This command will create the OCCAM stores and database. This will set up a new OCCAM node.

```
--test, -t : Builds a testing database
```

```
occam initialize
```

## login

This command will authenticate an account such that all subsequent commands will
be issued by this account. Give your username in the command and type in your
password when asked.

```
occam login wilkie

password for wilkie (typing will be hidden): _
```

## new

This command will create a new object of the given type. The new object's type
and name are given as arguments:

```
occam new <type> <name>
```

This creates a new simulator object in the directory `./simulator-my-sim`:

```
occam new simulator my-sim
```

This will automatically set up a `git` repository for the object history,
store the initial object in the object store, and create the initial
[metadata](/specifications/object.html)
that simply stores the type, name, and the newly created ID.

## pull

This command will discover an object at the given location or with the given id.

Pulls the object in the current directory:

```
occam pull .
```

Pulls the object from a particular URL:

```
occam pull https://bitbucket.org/occam/simulator-DRAMSim2
```

Pulls the object from whichever node has it:

```
occam pull 2223413a-92c6-11e4-b64f-001fd05bb228
```

## run

This command will execute an object.

```
--to,       -t : The object ID to execute
--revision, -r : The revision of that object
```

This command will run a very specific version of the given object:

```
occam run --to 2223413a-92c6-11e4-b64f-001fd05bb228 --revision 6e7b377796d22cdbd0b6a4b103ca5ebb3458002a
```

## search

This command will use the given keywords and search for any matching objects known to this node.

## set

This command will set a field in an object's metadata.

## status

This command will list information about the local path.

## sync

This command will change the local view of an object to the given revision.

## view

This command will list metadata for the given object.
