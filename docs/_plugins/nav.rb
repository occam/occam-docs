module Jekyll
  class NavTable < Liquid::Tag
    def initialize(tag_name, max, tokens)
    end

    def render(context)
      # Get the current page
      pageyml = context.environments[0]['page']
      pagetitle = pageyml['title'] || "Article"
      pagepath = nil

      puts "Page: #{pagetitle}"
      parser = nil
      context.environments[0]['site']['pages'].each do |k, v|
        if k.instance_variable_get('@output') == context.environments[0]['content']
          dir  = k.instance_variable_get('@dir')
          name = k.instance_variable_get('@name')
          pagepath = File.realpath(File.join('docs', dir, name))
          parser = k.converters[0].instance_variable_get('@parser')
        end
      end

      # Gather all navigation pages
      ret = ""
      Dir.foreach("docs") do |dir|
        path = File.realpath(File.join("docs", dir))
        if dir[0] != "." && dir[0] != "_" && File.directory?(path)
          if not ["js", "images", "fonts", "css"].include?(dir)
            # Open the index.md inside this path
            index = File.join(path, "index.md")
            if File.exists?(index)
              # Read yaml intro
              f = File.open(index, 'r')
              yml = YAML.load(f)
              title = yml['title'] || "Unknown"
              f.close

              url = "/#{dir}/index.html"

              # Append the item
              ret << "<li class='top'><a href='#{url}'>#{title}</a>"

              # Read all subentries
              ret << "<ul>"
              Dir.foreach(path) do |entry|
                if entry != "index.md"
                  subpath = File.join(path, entry)
                  if File.file?(subpath) && entry.end_with?('.md')
                    f = File.open(subpath, 'r')
                    subyml = YAML.load(f)
                    subtitle = subyml['title'] || "Article"
                    f.close
                    suburl = "/#{dir}/#{entry[0..-4]}.html"
                    classes = ""
                    if parser && subpath == pagepath
                      classes = "loaded"
                    end

                    ret << "<li class='#{classes}'><a href='#{suburl}'>#{subtitle}</a>"

                    if parser && subpath == pagepath
                      ret << parser.documentation_nav
                    end

                    ret << "</li>"
                  end
                end
              end
              ret << "</ul>"

              ret << "</li>"
            end
          end
        end
      end
      ret
    end
  end
end

Liquid::Template.register_tag('navtable', Jekyll::NavTable)
