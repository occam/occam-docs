---
layout: doc_page
title: Object Store
---

# Object Store: Distribution and Archival

OCCAM stores objects, which can be runnable programs, benchmarks, trace or
other binary data, program output, images, or structured data (and beyond!)
All objects have a unique identifier generated for them upon their creation.
Runnable objects have the ability to generate new data as part of their
operation. This output (including standard outputs) will also be stored.

The goal of OCCAM is repeatability. Therefore, all objects retain a history
of their changes and also relations to objects that they depend on, use in
some way, or the object that generated them. When you zoom out, objects are all
contained inside a potentially massive graph; connections going this way and
that.

Individual objects must be discoverable so that they can be repeated at any
time. Such repeatability is reliant on replication and distribution. When an
object is replicated on an OCCAM node, it can be thus discovered and pulled
down from that node. This is the spirit of our federation!

## Object Discovery

In the simple case, an object already exists. You simply need to point the
node in the direction of the object description. This is known as "discovery"
or "object import." You simply give OCCAM a URI of the object.

Upon discovery of that object,
the system can pull down the metadata describing that object. That is, what is
found in its [`object.json`](/specifications/object.html).
It will store the history of that
object in local storage. It will also store a record in the node's database of
the existance of that object and what inputs/outputs it can make use of.
This database record is used by the workflow editor so that it can know what
object interactions are possible.

OCCAM's configuration [`config.yml`](/architecture/config.html) maintains
the filesystem
location for all object history (current default: ~/.occam/objects). The history
of an object is stored as a git repository. The object is simply cloned from
either the canonical source or from a node that already knows the object. That
git repository will be cloned into a directory based on the type and uuid of
that object:

`~/.occam/objects/experiment/e4/b6/experiment-e4b69e2a-efdf-11e4-9748-001fd05bb228`

The first two pairs serve to divide the filesystem space to avoid overwhelming
path lookups and filesystem limitations. This filesystem path will be used to
pull out versions of the object metadata and configurations from previous points
in time.

## Object Provenance

Histories are kept of every change of an object. The metadata description of an
object is snapshotted using a git repository. From this repository, the system
can pull out any version of any file.

![border|!This object is currently looking at a view of itself from the past.](object_store/dramsim2.png)

In the above image, the object landing page is shown. This page contains a set
of links to previous and future versions of that object. If you navigate through
this history, you can see the object evolve or devolve accordingly. When an
object is used within an experiment, its revision is tracked within that
experiment and this revision is preserved beyond the control of the object's
owner. When that experiment is repeated, it is this purposeful retainment of
provenance that ensures that the same version of the object (the same code,
the same build script, the same name!) is used to satisify the experiment.

No matter the age of the object, or how much further it has been developed, nor
the difficulty of building and using the object will get in the way of its
repeatability. You can look at an old experiment, click through to the object
used to produce the data, and see the past before you: you can see the
configurations available to that version of the software and what inputs were
given to it. You can build and then interact with that old software.

All of this history is stored and replicated through the object store. And
ultimately kept locally in the location described in the previous section.

## Object Relations

It would be nice if all objects were self-contained, but in a practical setting,
objects depend on others to function. Supporting object relationships and
dependencies gives OCCAM a great deal of flexibility and power. There are
several types of relationships.

### Derivations

When an object derives another, it simply contains the second object within
itself. A typical example is a simulator that can also serve as a trace
generator. The same code generally serves both roles but invoked in drastically
different ways. So, as a simulator developer, you would simply write both
objects, but the second object would be contained in the first. The second
object is said to be *owned* by the first.

When objects are discovered, and if they contain a derived object, both objects
will be handled and stored in the system. When a derived object is used or
pulled from one node to the next, you end up pulling/invoking the parent node
instead.

### Dependencies

There are obvious dependencies within OCCAM. For instance, the structure of
defining experiments is hierarchical. You first define an underlying project
within a **workset** and then add/clone **experiments** on top of that. When
you do that, the experiment becomes a dependency of the workset.

Since those two are defined as connected, whenever you discover the parent (in
this case: the workset) you will also discover the dependencies (the
experiments) All will be pulled and replicated. Experiments will also mark any
objects it uses as dependencies. In the end, pulling or discovering a workset
is the extreme: many different objects may be discovered in the process.

### Generators

When objects are created, they will also tag the object acting as the creator.
For instance, an experiment will generate output (hopefully!) which you would
want to be tagged so that repeatability can be enforced.

When an experiment generates output, that output will tag the experiment and
its revision as it's **generator**. The experiment, in turn, will add that
output to the list: **generates** as part of its meta-data.

![border|!The output.](object_store/output.png)

In the above, the output was generated by experiment "baz." The revision is
also tagged. When you click on the link on that page, it will take you to the
exact description of the experiment that generated it. You could re-run that
experiment and compare the output, for instance. Or clone and tweak that
experiment and create new output. This is the power of OCCAM.

## Object Resources

Objects don't generally contain all of their source code, although they could.
Usually, object meta-data is distinct from normal development. The object tags
repositories, tarballs, etc as part of its installation process.

When OCCAM goes to build an object, it will pull down those resources. Since
they are generally going to be public URLs on more centralized sites, OCCAM does
not rely on them being available. Whenever a git repository is pulled, for
instance, that repository is stored as an object itself! (Which can be used
as input to another object, in case you are wondering) That object has its own
unique identifier and history.

With resources pulled in and made into objects, they retain their place in the
curated provenance and also maintain a reasonable level of availability: just
like any other object, they can now be pulled down and replicated from any OCCAM
node. Tarballs, binary files, git repositories... it doesn't matter.

The one problem area that we wish to solve is for resources pulled down during
a build phase. Some objects, which fear consolidated distribution, will pull
down the source code of derivative tools during their builds. OCCAM does not
currently account for network access during this time. One proposal is to simply
save the network traffic during the build phase, store that as a dependent
object, and replay that traffic during a subsequent build phase. This would
make use of a network proxy within the VM environment of the build process.
HTTPS would have to be handled separately (or time would have to be repeated
to avoid nonce and timeout issues that SSL would use to counter a repeated
packet attack) Not the most trivial problem, but would make the repeatable
build system extremely robust.

## Special Objects

Certain object types are used by OCCAM for various special features. Worksets,
Groups, Experiments, etc are all handled specially. All other objects are
unregulated outside of the standard fields within
[`object.json`](/specifications/object.html).

### workset

This is the root container for all experimentation. They are also maintained
in the Workset database table.

### group

This is an object that may contain other groups and experiments. It is used to
create a structural hierarchy for one's work. They are also maintained in the
Group database table.

### experiment

This contains a workflow and a set of configurations and generally describes
some work one wants to perform. Experiments generate `occam/task` objects that
eventually get dispatched and executed. They are also maintained in the
Experiment database table.

### person

This contains metadata about an individual person that uses the system. People
can also be replicated across nodes so that their presense and information can
be available in a decentralized manner. They can only log into servers which
they explicitly have access to, however. They are also maintained in the Person
database table.

### occam/git

This wraps a git repository and is generally created when an object requests the
installation of a git repository as a resource. The path that these ultimately
reside is defined by the configuration in
[`config.yml`](/architecture/config.html).

### occam/task

This is an individual run that can be executed by a worker node by the OCCAM
dispatcher. These are the objects that worker nodes pull down and execute. They
are generated from **experiment** objects.

### occam/runnable

This is a special type that can be used in place of any object that has run
metadata described for it. When objects list this as a possible input, any
runnable object can be used to resolve this type.
