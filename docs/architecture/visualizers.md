---
title: Visualizers and Plotters
layout: doc_page
---

# Visualizers and Plotters

## Motivation

## Sandboxing

The OCCAM web server will show the widget in the browser as embedded within
other content. The person viewing the widget will be able to interact with the
widget fully, but the widget itself can be arbitrary code and therefore must be
isolated somewhat from being able to read privileged information or be able to
act on that person's behalf in any arbitrary way.

![border|The interactive widget or visualizer can be embedded within other content.](/images/visualizers/plotter-embedded.png)

The way this works is through sandboxing the widget within the browser. We use
[iframe sandboxing](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#attr-sandbox)
provided by HTML5 with the "allow-scripts" capability. With
this, the embedded widget cannot 1. Submit forms, 2. Popup windows, 3. Consider
itself the same origin as the rest of the site, or 4. Navigate the browser away
from this page. This is already on top of the normal iframe restriction that it
may not inspect or change the parent's document.

Since a sandboxed iframe cannot see or interact directly with the parent page,
it cannot retrieve data or configurations directly from the page it is embedded.
Instead, there is a message passing API that
widgets can use to send and receive information about the data or
configuration of the widget.
This uses the javascript
[postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
API. The widget will need to write a message handler to receive data and use the
provided OCCAM Interactive Widget API to send messages to request information about particular
configuration options and datapoints.

## API

Interactive Widgets use the javascript
[postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
capability to update the configuration and data being used by the widget as it
is changed.

An Interactive Widget has two sets of data that can be manipulated by somebody
and sent to the widget and rendered or analyzed:

1. Configuration Data
2. Datapoints

An example widget may have handling code that looks like this:

{% highlight javascript %}
var configuration = {};
var data = {};

window.addEventListener('message', function(event) {
  var message = event.data;

  // Based on event, set global data
  if (message.name === 'updateConfiguration') {
    // Retrieve the new set of configuration options.
    configuration = message.data;

    // (Optional) Retrieve new datapoints (always assuming it changed)
    window.parent.postMessage({
      'name': 'updateData',
      'data': message.data
    }, '*');
  }
  else if (message.name === 'updateData') {
    // Retrieve the new set of datapoints.
    data = message.data;
  }

  // Here, you can redraw or react however you would like...
});
{% endhighlight %}

When receiving updated configuration, it is up to the developer of the widget
to decide how to best render that information. Furthermore, it may be wise to
determine which options have changed in order to be most efficient. For
instance, not redrawing the entire widget when some label is updated and
instead only updating the existing element.

In this regard, receiving updates from the OCCAM page may be as simple as
copying the entire configuration record over and redrawing, or as complicated
as one can imagine.

### updateConfiguration

When the widget receives this message, it will contain the full configuration
within the `data` field of the message. Assume the widget object has these
configurations within its [object metadata](/specifications/object.html):

{% highlight javascript %}
{
  'configurations': [
    {
      'name': 'foo',
      'schema': {
        'item': {
          'type': 'int',
        }
      }
    },
    {
      'name': 'bar',
      'schema': {
        'another_item': {
          'type': 'int',
        }
      }
    }
  ]
}
{% endhighlight %}

When it receives an `updateConfiguration` message, the message will look like
this (where the actual data is just a placeholder, and will be given by the
person using the widget):

{% highlight javascript %}
{
  name: 'updateConfiguration',
  data: {
    foo: {
      item: 4
    }
    bar: {
      another_item: 6
    }
  }
}
{% endhighlight %}

The data received will correspond directly to the
[configuration schema](/specifications/input_schema.html) given in the
`configurations` section of the widget. Any datapoints, however, will just
consist of their metadata. See the `updateData` section below for information
on resolving datapoints.

### updateData

This message will request the server to retrieve the actual datapoints for the
given configuration options. Place the configuration data in `data` and it will
eventually post an `updateData` message to the widget containing a copy of the
original message appended with a `datapoints` field containing the corresponding
data from the requested object.

To expand on the purpose of this message and its relation to
`updateConfiguration`, it is useful to see the whole process from updating the
configuration form to ultimately gathering new datapoints to render in the
widget.

Whenever datapoints are appended to a configuration field or group, the actual
data is postponed until that data can be retrieved. Or it may only be necessary
to retrieve that data at a particular point in time or just a subset of the
data if the pool of data is large.

Therefore, an `updateConfiguration` will be sent instead detailing the range
and location of the data. That is, only metadata is sent. The widget can then
request that data as it sees fit. When such an `updateData` request is made to
the OCCAM page, the actual data is returned in this `updateData` message in the
`datapoints` field.

{% highlight javascript %}
{
  'groups': [
    {
      'graph_type': 'line',
      'datapoints': [
        {
          'object': {
            'id': "075cf1e4-b5d8-11e5-a238-001fd05bb228",
            'revision': "fa3f277513d9bc6d28cf4fddd0dd83d75b188e11"
          },
          'nesting': [
            'channels',
            [0],
            'ranks',
            [0],
            'banks',
            [ [ 0, 7 ] ],
            'bandwidth'
          ]
        }
      ]
    }
  ]
}
{% endhighlight %}

In the example above, this would be what you would receive from an
`updateConfiguration` message. These are configuration options. Under the
'groups' section, you have an array of groups. In the lone group represented
here, you have 2 pieces of configurable data: *graph_type* and *datapoints*.
Of interest here is the *datapoints* key, which has the metadata necessary to
denote what data should be pulled out and where.

The `object` key for a datapoint gives you the id and revision of the object
whose data we will refer to under `nesting`. The nesting key then gives us a
path through the structured data to filter the data to exactly what we want
to pass to the widget.

![border|This data corresponds to the requested datapoints in the metadata above.](/images/visualizers/plotter-dataview.png)

In this case, we look at the data (in that object at that point in time) and
look at the first element of the *channels* key (which is an array in this
case), the first element of the *ranks* key, and then pull out a range of data
from the first 8 elements of the *banks* key filtering for the *bandwidth*
values.

Therefore, if we post a request for the data using that configuration data
above, our `updateData` message will look (assuming the data depicted
above) like this:

{% highlight javascript %}
{
  'groups': [
    {
      'datapoints': {
        'values': [ 0.207, 0.143, 0.302, 0.191, 0.389, 0.318, 0.318, 0.207 ],
        'type':   'float',
        'units':  'GB/s'
      }
    }
  ]
}
{% endhighlight %}

Notice that the datapoints also gives you the hinted metadata based on the
[structured output schema](/specifications/output_schema.html) given by the
object we pulled the data from. Specifically, you will also receive a `units`
field and a `type` field, if available and known.
Also note that the structure returned will line up with the structure of the
[configuration schema](/specifications/input_schema.html) for the widget.

## Efficient Datapoint Retrieval

All data must be requested by the widget. Whenever a configuration option changes
the widget will be notified along with the new set of configuration options.
Within these options are datapoint fields whose values are an object and a range
of values from that object. The actual value of these fields is that data
itself, but it is deferred until requested at a later point.

Therefore, whenever a configuration changes, the data may not need to be pulled
again. It is up to the widget itself to determine when it may need to resolve
datapoints and how many or what range to limit that request.

You can, however, just request the data at every request. This is the
approach taken in the example code leading the API section above. This is
probably acceptable when there are going to be very few datapoints or very
few configuration changes. However, you don't generally want to pull in data
every time somebody changes the text color or updates the title of a graph, for
instance. In these cases, it is wise to implement a solution that can determine
when the data you have already resolved is stale and reload that data only
when you need to.

Let's assume that our configuration data, when it arrives, is formatted just
like our example for `updateData` above:

{% highlight javascript %}
{
  'groups': [
    {
      'graph_type': 'line',
      'datapoints': [
        {
          'object': {
            'id': "075cf1e4-b5d8-11e5-a238-001fd05bb228",
            'revision': "fa3f277513d9bc6d28cf4fddd0dd83d75b188e11"
          },
          'nesting': [
            'channels',
            [0],
            'ranks',
            [0],
            'banks',
            [ [ 0, 7 ] ],
            'bandwidth'
          ]
        }
      ]
    }
  ]
}
{% endhighlight %}

In this situation, changing the *graph_type* won't affect the staleness of our
data. Once we get all of the graph data once, we should be able to change our
graph type from "line" to "bar" without having to pull the data again. This
will save us from an expensive request to the server and a noticeable lag.

So we can write our handlers in our widget to determine when the datapoints change instead:

{% highlight javascript %}
var configuration = {};
var data = {};
var dataCache = [];

window.addEventListener('message', function(event) {
  var message = event.data;

  if (message.name === 'updateConfiguration') {
    configuration = message.data;

    var stale = false;

    // For every element in the groups array, check the datapoints field
    for (var i = 0; i < configuration.groups.length; i++) {
      var test = JSON.stringify(configuration.groups[i].datapoints);

      // If it is a new group or an updated group, mark as stale data
      if (i >= dataCache.length || test != dataCache[i]) {
        stale = true;
        break;
      }
    }

    // If we have any stale data, pull all the data
    if (stale) {
      window.parent.postMessage({
        'name': 'updateData',
        'data': configuration
      }, '*');
    }
  }
  else if (message.name === 'updateData') {
    // This is the configuration data used to retrieve the datapoints
    var input = message.data;

    // Capture the new data
    data = message.datapoints;

    // Update the data cache to mark what our data is now
    for (var i = 0; i < input.groups.length; i++) {
      if (i >= dataCache.length) {
        dataCache.push("");
      }
      // Store the metadata into our cache to reflect the fact that our data
      // is based on this metadata
      dataCache[i] = JSON.stringify(input.groups[i]);
    }
  }

  // redraw or reanalyze data
});
{% endhighlight %}

This way, if the updated configuration does not touch the datapoints section,
the widget will not overreact and pull down the data needlessly. The data will
be the same if it did.

However, depending on what the widget does, this could still be improved quite
a bit. In this case, it still pulls all data. It could further prune the request
by only asking for the data for a subset of the configurations by removing
those that match and then going through the response and merging the data with
what the widget already has. (In contrast, the implementation above always
overwrites all of the data when any datapoint changes.)

As the needs of the widget require more and more data to be pulled in at one
time, these considerations should be considered and the complexity added to handle this case.

## Metadata

## Provenance
