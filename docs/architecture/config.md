---
layout: doc_page
title: Configuration File
---

# Config.yml: OCCAM Configuration

When you run `occam initialize` for the first time, the default `config.yml` is
generated in `~/.occam/config.yml`. This file is used by all OCCAM services to
determine how data is stored and where objects are placed. If you change the
paths, your node will *forget* where some objects are!

For the example `config.yml` that is prepared for a default install, look in the
[occam-worker repository](https://bitbucket.org/occam/occam-worker/raw/e05eaeb8b221bd6433bf0a76ebc9757f4ad2eb28/config.yml.sample).

## Postgres Support

### adapter

This should be "postgres"

### uri

This is the uri to the postgres database. For instance `postgres://localhost/occam`

### user

The postgres user that has access to the specified database.

### password

The password for the given postgres user.

## SQLite3 Support

### adapter

This should be "sqlite3".

### path

This should be the path to the database. It will default to a reasonable path within `~/.occam`, so generally speaking the only thing you need to do is specify the adapter.

### timeout

The timeout for waiting for a lock to be released in the database manager. If you see a lot of lock contention, you may wish to increase the value to something like 1000. It is in milliseconds.

## Custom Pathing

OCCAM takes and generates and stores a lot of data. Some of it is more sensitive or more important than other data. For this reason, OCCAM allows you to specify where that data ultimately lives on your system based on the category of data. For instance, "objects" and "git" are data that is generally more permanent and represents the history and manual labor of some work. Therefore that would be better stored on something fast and robust to failure. The "runs" section is temporary data for building virtual machines, and it may be fine to have less robustness on the type of storage it uses.

### paths/objects

Where object histories are stored.

### paths/git

Where git objects and git repositories, which were object [installation resources](object_store.html#object_resources), are stored.

### paths/store

Where file resource data is stored. In OCCAM, objects can say that they are using a file found via a URL. The first time this file is retrieved, it will be retrieved via the network and then stored as an Object. This file can then be retrieved from an OCCAM node instead of the resource it originally came from, which solves the problem of the lack of data permanence of hyperlinked resources on the Internet.

Files will be stored here, which can be arbitrarily large.

### paths/builds

This is where builds of objects are staged. When OCCAM builds an object, it will put the object (at the revision requested) in a directory created here. It will unpack and put any resources and dependent objects within this directory as well and build through a VM backend (Docker, VirtualBox, etc)

These paths are generally temporary, but may be arbitrarily large as a footprint. They may also persist if the builds are incremental. That is usually the case if you are testing or developing actively through OCCAM.

### paths/jobs

This is the path where job data for the worker queue is staged. When jobs are pulled off the queue, their metadata and progress is stored here. In case a worker fails, there will be a record of progress to use to pick off from.

### paths/runs

This is the path where virtual machines are built and executed from. Some workloads may require some area of disk to be mounted or shared. This is done here. These paths are instanced for a particular run of a workflow, and thus are temporary and can be cleaned up after a run.

Objects that are generated by a running virtual machine of a workflow will be generated here. These can be arbitrarily large.

### paths/nodes

This path stores metadata for known OCCAM nodes that exist outside of our network.

## Security

### passwords/scheme

Which password hashing scheme. The only option right now is 'bcrypt'.

### passwords/rounds

The number of rounds for a round-based password scheme.

### secret

The secret private key used to encrypt server session data. OCCAM, upon
initialization, will generate a good, unique key for this purpose for you.
However, if you feel you have leaked the key or need a new one for any
reason, you can set it here. The new secret key would only be valid if
services are restarted! (This would obviously invalidate any open sessions)
