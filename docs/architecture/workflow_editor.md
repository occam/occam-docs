---
title: Workflow Editor
layout: doc_page
---

# Workflow Editor

One of the main practical purposes of OCCAM as a living archive is to enable faster
experimentation with applications and software tools. The ambition is to lower
the barrier to entry for both research and play.

Special OCCAM objects called "experiments" can be set up to compute based on
a collection of objects chained together. It captures the essence of UNIX style
pipes in a way that can be repeated through wrapping the process into one or
more virtual machines. Essentially, you give it an application to run, attach
some input, and you can tell it to run this *workflow* and it will do the
work of dispatching and executing things as it needs to. It will even do some
tasks in parallel or in distinct environments if it needs to do so.

A **workflow** is the name of this directed graph. It is stored as part of an
experiment object's metadata (though technically any object can have a workflow)
The Workflow Editor consists of a widget that both views a workflow and, if
the logged in account has permissions, edits the workflow. Every change made to
the workflow is appended to the object's history such that any output made
by the workflow will appropriately tag the object at that point in time.

## Motivation

The widget must do several things.

* Must easily find objects that are pertinent to the workflow as it is being built.
* Visualize well the relationship between objects and how the work and data will flow.
* Maintain provenance by tagging all objects with revisions and ensuring created objects point back to the exact workflow that created it.

## Metadata

The workflow information is stored within the object metadata within the `workflow` key.
It is represented simply as a list of objects. Each object in the list will have a `to`
key which says which object the output will be directed toward.

![A simple workflow with a single object matching the following metadata.](/images/workflow_editor/workflow-editor-simple.png)

{% highlight javascript %}
{
  "workflow": {
    "connections": [
      {
        "object": {
          "revision": "6e7b377796d22cdbd0b6a4b103ca5ebb3458002a",
          "id":       "2223413a-92c6-11e4-b64f-001fd05bb228",
          "type":     "simulator",
          "name":     "DRAMSim2"
        },
        "to": -1
      }
    ]
  }
}
{% endhighlight %}

Above, you can see the object represented with enough information to exactly replicate it.
The `object` key lists the [object metadata](/specifications/object.html) for the connected object.
In this case, this simulator is tagged with the revision that will be used when
[virtual machines are created](/architecture/worker_queue.html)
for the workflow.
The `id` key would indicate the next node in the workflow by the index
within the `connections` list. In this case, this is the last (and only) node
in the workflow. Therefore, the `to` key is `-1`.
All nodes with `to` being `-1` would be considered terminal or tail nodes,
which after being performed would mean the resolution of the workflow.

![A more complex workflow with 3 objects.](/images/workflow_editor/workflow-editor-complex.png)

{% highlight javascript %}
{
  "workflow": {
    "connections": [
      {
        "to": -1,
        "object": {
          "name":     "HMMSim",
          "id":       "486623a6-dc01-11e4-afe4-08002745ce1b",
          "type":     "simulator",
          "revision": "37e91371021b6f34c5dc1dfe6447df21739b85cc"
        }
      },
      {
        "to": 0,
        "object": {
          "name":     "generated",
          "created":  true,
          "type":     "trace"
        }
      },
      {
        "to": 1,
        "object": {
          "name":     "TracerPin",
          "id":       "8df56c9c-de63-11e4-8b49-08002745ce1b",
          "type":     "trace-generator",
          "revision": "7b8076ad55b107b3dfd2939b70cc64b1ac608999"
        }
      },
      {
        "to": 2,
        "object": {
          "name":     "Aqsios2.0",
          "id":       "2a99b0ba-dd99-11e4-8224-08002745ce1b",
          "type":     "DSMS",
          "revision": "bbe14bac5b50676cf38e28911206104a20e496d3"
        }
      }
    ]
  }
}
{% endhighlight %}

## Provenance

## Example
