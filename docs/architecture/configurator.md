---
layout: doc_page
title: Object Configurator
---

# Object Configurator: Interactive Configuration and Input

OCCAM tools can often be complex tools and simulators which historically have had a diverse range of methods of input and configuration.
Seeing the inherent issue of usability and accessibility, OCCAM offers an optional method of configuring tools through an interactive widget.
This widget can be written independently of the tool itself and its authors.
In fact, more than one can be available without stepping on the toes of others.
If your particular workflow benefits, you may even write your own for very specific uses.

## Motivations

* Randomization of inputs
* Helpful visualizations to aid understanding of how options affect the tool
* Building a tool to import configurations from other formats
* Quick client-side validation or restriction of input

## Sandboxing

The OCCAM web server will show the widget in the browser next to the traditional
web form for configuration. The experimenter will choose the configurator which
will load the widget and any changes the widget makes will update the normal
form.

The way this works is through sandboxing the widget within the browser. We use
[iframe sandboxing](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#attr-sandbox)
provided by HTML5 with the "allow-scripts" capability. With
this, the embedded widget cannot 1. Submit forms, 2. Popup windows, 3. Consider
itself the same origin as the rest of the site, or 4. Navigate the browser away
from this page. This is already on top of the normal iframe restriction that it
may not inspect or change the parent's document.

Since a sandboxed iframe cannot see or interact directly with the parent page,
it cannot update the form directly. Instead, there is a message passing API that
widgets can use to send and receive information about the configuration fields.
This uses the javascript
[postMessage](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
API. The widget will need to write a message handler to receive data and use the
provided OCCAM widget API to send messages to request information about particular
configuration options.

## API

### Introduction to postMessage

OCCAM widgets use the
[postMessage API](https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage)
to explicitly communicate between the sandboxed widget and the normal content
of the page. The API is oddly asynchronous. When you post a message to make a
request, the result will be returned soon after, but at an ultimately
indeterminate time through a posted message handled by an event listener.

Therefore, the widget code must set up both a handler for incoming messages and
also write code to send those messages. Both can be done rather simply, and
included with the descriptions of the various requests that can be made,
working javascript code is given to quickly get up to speed.

Every message is formatted the same way. An object is sent to the parent
window of the widget. Within that object, the **name** field will contain the
name of the request. For example, `getValue`. The remaining fields will consist
of any arguments required to fulfill that request. Generally there will be a
field for the `index` of which configuration form to look at and `key` which
consists of a string identifying the exact option desired. The following sections
describe the various requests that can be made.

In the source code for the web server, the code and mechanisms that drive the
widget messaging can be found in [public/js/occam/configurator.js](https://bitbucket.org/occam/occam-web/raw/79241583adc82adb291ca614f0631099869deb61/public/js/occam/configurator.js)

### setValue

The `setValue` message will update the configuration form to reflect a new value when given the index and key of that configuration item.

Here is an example of how to write a function that wraps the postMessage API:

{% highlight javascript %}
var setValue = function(index, key, value) {
  window.parent.postMessage({
    'name':  'setValue',
    'index': index,
    'key':   key,
    'value': value
  }, '*');
};
{% endhighlight %}

### getValue

The `getValue` message will return the current value of the requested configuration
option in the configuration form.

Since `getMetadata` also returns the current value, it may be easier to make use
of that message instead.

Here is an example of how to write a function that wraps the postMessage API and handle the response asynchronously:

{% highlight javascript %}
var getValue = function(index, key, callback) {
  var queuedEvent = function(event) {
    var data = event.data;

    if (data.name === 'getValue' && data.key === key && data.index === index) {
      var value = data.value;
      callback(value);
      window.removeEventListener('message', queuedEvent);
    }
  };

  window.addEventListener('message', queuedEvent);

  window.parent.postMessage({
    'name':  'getValue',
    'index': index,
    'key':   key
  }, '*');
};
{% endhighlight %}

The data you will get will look something like this:

{% highlight javascript %}
event.data = {
  index: 0,
  key:   "total_row_accesses",
  name:  "getValue",
  value : 4
}
{% endhighlight %}

The sent message will be repeated with a new key `value` which will contain the
content currently in the configuration form.

### getMetadata

The `getMetadata` message will pull out the metadata for the given configuration
item from the schema. The schema data will be returned in a posted message with
the `data` property of the event.

Here is an example of how to write a function that wraps the postMessage API and handle the response asynchronously:

{% highlight javascript %}
var getMetadata = function(index, key, callback) {
  var queuedEvent = function(event) {
    var data = event.data;

    if (data.name === 'getMetadata' && data.key === key && data.index === index) {
      callback(data);
      window.removeEventListener('message', queuedEvent);
    }
  };

  window.addEventListener('message', queuedEvent);

  window.parent.postMessage({
    'name':  'getMetadata',
    'index': index,
    'key':   key
  }, '*');
};
{% endhighlight %}

In the function above, the given callback will be called with the option's
metadata. Also included will be the current value of the requested option.
The data you will get will look something like this:

{% highlight javascript %}
event.data = {
  index: 0,
  key:   "total_row_accesses",
  name:  "getMetadata",
  info:  {
    description: "The maximum...",
    label:       "Total Row Accesses",
    options:     [],
    type:        "int",
    value:       4
  }
}
{% endhighlight %}

In this, we can see the request is repeated with a new `info` entry containing
the exact schema entry the was originally used to generate the configuration
form. The `options` field will be populated when the option is an enumerated
(that is, a dropdown option) field. It will contain the set of possible options.
