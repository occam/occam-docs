---
layout: doc_page
title: Worker Queue
---

# Worker Queue: Job Scheduling

OCCAM allows you to build experiments by specifying a workflow and a set of
configurations. From these configurations, a set of tasks is generated that can
be executed or dispatched for completion via a job queue.

Let's use an example workflow:

![!Workflow with 5 components](worker_queue/workflow.png)

Here we have a database connected to a trace generator which creates a trace.
And then, following through, that trace connects with a memory simulator which
produces some data.

## First Step: Run Generation

Source file: [[lib/run_builder.py](https://bitbucket.org/occam/occam-worker/src/df3110ca3b0cbee9676889ee7b15ff8990093762/lib/run_builder.py?at=develop)]

For each configuration in that workflow, all possible permutations of configuration
options will produce a **run**. That is, when you specify that you wish to sweep an
option:

![!The configuration form showing a sweep on a parameter](worker_queue/sweep.png)

In the above case, the instruction cache can only be specified in powers of two for
this simulator. Therefore, a sweep from 32 to 64 would result in two runs of the
workflow each for 32 and 64.

OCCAM will generate seperate but complete configurations for all possibilities. For
each of those, it iterates the next step of the task generation process: Job
Generation.

## Second Step: Job Generation

Source file: [[lib/task_builder.py](https://bitbucket.org/occam/occam-worker/src/df3110ca3b0cbee9676889ee7b15ff8990093762/lib/task_builder.py?at=develop)]

With each complete configuration, OCCAM can complete a run of the workflow. A
given workflow may be complex and involve a series of steps that have to be
completed in order or even simultaneously.

There are a few rules for how OCCAM determines job division. The workflow is
walked from the first nodes (Aqsios in our example) to the ultimate node (data)
and determines if that step requires being executed. In our workflow example,
Aqsios is an application and would normally run, however, the next node that it
feeds into is a trace-generator. Therefore, this trace-generator and *not* OCCAM
will run our database application within its own process.

OCCAM will understand this, and will only create a job for the trace-generator.
The job will initialize the database application and then only execute the
tracer.

The database application, like all runnable objects, contains a script that
will report the command this will execute it. In normal circumstances, OCCAM
could simply run that command, however it is given to the tracer who will run
it themselves. In this fashion, the trace-generator could be used as input to
itself without requiring any change to the object to handle that!

Once it walks the workflow and discovers the generated node, it knows it must
wait until the tracer finishes its work before that trace can be passed to
its ultimate destination: the simulator itself. OCCAM will generate a job
for the simulator and mark it dependent on the job for the tracer. Within the
job dispatcher, it will only dispatch the simulation job once the tracer job
has successfully finished.

## Third Step: Job Dispatch

Source file: [[lib/worker.py](https://bitbucket.org/occam/occam-worker/src/df3110ca3b0cbee9676889ee7b15ff8990093762/lib/worker.py?at=develop)]

Once OCCAM has generated all jobs necessary for a run, it will either run them
directly or dispatch them via a worker queue. This "queue" is technically more
versatile than a normal FIFO-queue. Its intention is FIFO, but due to
dependencies and resources, it may reorder the jobs.

When a worker is idle, it will pull down a job that is ready to run. It will
clone the workset and experiments on that worker node. Once it has done this,
it can run the workload described by the task built during Job Generation (the
second step.)

If a job has co-running tasks (also described as co-dependent jobs), they must
all be executed at the same time within the same worker node. Therefore, all of
these jobs must be taken off the queue at nearly the same time and claimed by
only a single worker node. When this happens, all tasks are cloned on that
worker node in the same location so they can coordinate, and then all tasks are
executed.

Co-running jobs share information about which processes are running alongside
them and can elect to synchronize. This is done via a shared lock on the
filesystem that is mounted within each virtual machine. When a command is
executed for an object (such as Aqsios in our example) it will amend the
shared lock with its status and wait until the lock suggests all running
objects have also started execution similarly. At that point, all processes
should start executing in nearly the same time (depending of course on system
resources and typical concurrency luck.) It is up, at that point, to the program
developers to have written sound concurrent code when necessary.

## Fourth Step: Output Raking

Source file: [[lib/commands/run.py](https://bitbucket.org/occam/occam-worker/src/df3110ca3b0cbee9676889ee7b15ff8990093762/lib/commands/run.py?at=develop)]

Once a job has completed, the output generated by that job is raked and stored
within the OCCAM object store. Then it can be pulled down and used by any jobs
which depend on it either immediately or far in the future.

When an output is generated, its object stores a tag to the object and revision
that generated it. In this case, the **experiment** revision with our workflow
is tagged. To repeat the process on a different OCCAM system, all one would
need to do is pull down that experiment at that revision and re-dispatch the
same workflow. All output will contain a tag to its origin. In the following
diagram, that would be experiment "baz."

![border|!The output retains a link to the object that generated it.](worker_queue/output.png)

The experiment will also be updated to contain a reference to its output. This
will, in turn, update any groups or worksets that are the parent of the
experiment. Whenever the experiment's configuration or workflow are updated,
the output listing will be cleared. Old output will be preserved, however,
along with the history of that experiment so that it may be reviewed or repeated
in the future.
