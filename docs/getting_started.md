---
layout: doc_page
title: Getting Started
---

# Getting Started: Creating an OCCAM Object

This article discusses the process of wrapping an existing or new object for use with OCCAM.

## Meta-data (object.json)

{% highlight javascript %}
{
}
{% endhighlight %}

## Describing Install Process

### Write Descriptor

{% highlight javascript %}
"install": {
  "git": "git://github.com/dramninjasUMD/DRAMSim2.git",
  "to":  "package"
}
{% endhighlight %}

### Test

{% highlight bash %}
occam install
{% endhighlight %}

You should see the specified package delivered to that path. It should now have assigned an 'id' to it as well.

## Describing Build Process

### Write Descriptor

{% highlight javascript %}
"build": {
  "using":    "base-66249f7e-9478-11e4-ac02-001fd05bb228",
  "script":   "build.sh",
  "language": "bash"
},
{% endhighlight %}

### Test

{% highlight bash %}
occam build
{% endhighlight %}

This will spawn a VM and build the project based on running your specified script. You can see all output written to the terminal. If there are errors, correct them, and retry.

{% highlight bash %}
occam update
{% endhighlight %}

The `update` command will not completely re-do the build, but rather attempt to patch changes and rebuild. Generally, this is good for when source files are changed, but it can also append changes to build scripts without deleting in-progress builds.

## Describing Run Process

### Write Descriptor

{% highlight javascript %}
"run": {
  "script":   "launch.py",
  "language": "python",
  "version":  "3.3.0"
},
{% endhighlight %}

### Test

{% highlight bash %}
occam run
{% endhighlight %}

This will run the process using the script you have specified.

## Describing Input (input_schema.json)

### Write Descriptor

{% highlight javascript %}
"configurations": [
  {
    "file":   "input.json",
    "schema": "input_schema.json",
    "name":   "DRAMSim2 Options",
    "type":   "application/json"
  }
],
{% endhighlight %}

### Test

{% highlight bash %}
occam configure
{% endhighlight %}

This will list the default configuration options.

## Describing Outputs

### Write Descriptor

{% highlight javascript %}
"outputs": [
  {
    "file":   "output.json",
    "schema": "output_schema.json",
    "type":   "application/json"
  }
],
{% endhighlight %}

### Test

{% highlight bash %}
???
{% endhighlight %}

## Describing Structured Output (output_schema.json)

### Write Descriptor

{% highlight javascript %}
{% endhighlight %}

### Test

{% highlight bash %}
???
{% endhighlight %}

This will produce an example output with random results.

Alternatively, just run your object with the default inputs:

{% highlight bash %}
occam run
{% endhighlight %}
