---
title: Output Schema
layout: doc_page
---
# Output Schema

OCCAM provides a mechanism to generate graphs and understand results agnostic to the actual simulator used.
To do this, each simulator describes the possible results it *may* generate as a `json` schema.
Data is typically hierarchical.
The schema simply represents each value where it would appear in this hierarchy.

Each data point is described with its type (int, float, etc) and its units.
Knowledge about the type of value will aid in the production of correct and relevant graphs and visualizations.

Here is an example excerpt `output_schema.json` file for [DRAMSim2](https://wiki.umd.edu/DRAMSim2) which you can also find in our [code repository](https://bitbucket.org/occam/occam-dramsim2/src):

{% highlight javascript %}
{
  "channels": [
    {
      "id": {
        "type": "int",
        "label": true
      },
      "bandwidth": {
        "type": "float",
        "units": "foo"
      },
      "ranks": [
        {
          "writes": {
            "type": "int"
          },
          "writesSize": {
            "type": "int",
            "units": "bytes"
          },
          "reads": {
            "type": "int"
          }
          "readsSize": {
            "type": "int",
            "units": "bytes"
          },
          "latency": [
            {
              "max": "int",
              "amount": "int",
              "min": "int"
            }
          ],
        }
      ]
    }
  ]
}
{% endhighlight %}

In this example, the simulator will output its results as a dictionary object. For DRAMSim2, there is a script that will run after the simulation is complete that will parse the specific output so that it corresponds to this schema. This script is unique to every simulator and must be written if the simulator does not natively output JSON to match its provided OCCAM schema.

The key `channels` is an array of data.
Each channel element contains an `id`, a float representing `bandwidth`, and an array of `ranks`.
Ranks in turn each contain a number of `writes` and the number of `bytes` these writes represent.
Each rank contains an array of `latency` tuples consisting of a `max`, `amount`, and `min` which are all integers.

When an array is expected, you use an array object in the json.
It will always have only one item in it: the description of what type of element to expect in the array.
Arrays, as such, can only represent a single data-type.

A Hash (key/value) object is specified as a json dictionary object.
In the above, `channels` is an array of hash objects containing values for `id`, `bandwidth` and `ranks` keys.

Actual data-types are the leaves of this hierarchical representation.
Essentially, an object with a `type` key with a string value describes a data-type and not a Hash object.

Data-types can be described as simply a name and then a type:

{% highlight javascript %}
"reads": {
  "type": "int"
}
{% endhighlight %}

This creates a data-type called 'reads' expected a value of an integer. More complicated data-types can be created from optional values described later on in this document:

{% highlight javascript %}
"writesSize": {
  "type": "int",
  "units": "bytes"
}
{% endhighlight %}

Here, `writesSize` is an integer that represents a number of bytes.
Unit information is critical for ensuring accurate visualizations and avoiding human error.

{% highlight javascript %}
"latency": [
  {
    "max": {
      "type": "int"
    },
    "amount": {
      "type": "int",
      "units": "nanoseconds"
    },
    "min": {
      "type": "int"
    }
  }
]
{% endhighlight %}

In this example, `latency` is an array of objects with three integer fields.
The `amount` field has units in nanoseconds.
Descriptions of each parameter used to define data-types are listed below.

## Type

### int
### float
### string

## Units

A free-form string that describes the unit (as a plural) for this data.

{% highlight javascript %}
"averagePower": {
  "type": "float",
  "units": "watts"
}
{% endhighlight %}

The above describes `averagePower` as being a floating point value determining the number of `watts` in average is being consumed.
When graphs are produced from this value, the default label for the axis representing the data will describe the units.
The presence of this field will make it less likely for human error to misrepresent data.

## Label

A boolean value.
When `true`, then this data type is typically not used as a data-point, but rather as graph labels.
