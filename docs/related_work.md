---
layout: doc_page
title: Related Work
---

# Related Work

This is a list of similar or overlapping projects to OCCAM. Alongside names of these projects, there is a website link and a description which is copied from their own pages. The "thoughts" sections are our own words specifying contributions and other descriptive facets of that project.

## Digital Computation Archives

### OLIVE

**Website**: [Available here](https://olivearchive.org/).

**Description**: Olive is a collaborative project seeking to establish a robust ecosystem for long-term preservation of software, games, and other executable content. Born at Carnegie Mellon University, Olive addresses the current gap in preservation technology by providing a curated environment for the preservation and distribution of executable content.

The Olive client software, called VMNetX, runs on Linux and uses the open-source KVM virtual machine monitor. VMNetX can execute virtual machines directly from any webserver; no special server software is required. VMNetX is developed on GitHub and released under version 2 of the GNU General Public License.

Binary packages are available for Debian, Fedora, Red Hat Enterprise Linux, and Ubuntu. Windows users can install the thin-client version of VMNetX to interact with virtual machines launched in the cloud through our web-based service. Source releases of VMNetX and its Windows dependencies are also available.

The Olive site software is not yet publicly released, but will eventually be made available under version 2 of the GNU General Public License.

**Thoughts**:

* A Client application can download and run VMs which are stored on a server.
* VM changes persist on client machines.
* Technology was developed to stream VM content such that the entirety of the VM is not needed to start the VM.
* Changes/modifications to VMs are done exclusively on the client are not seen or archived on the server.

### Open Science Framework

**Website**: [Available here](https://osf.io/).

**Description**: Project management with collaborators, project sharing with the public.

The Open Science Framework (OSF) supports the entire research lifecycle: planning, execution, reporting, archiving, and discovery.

### RunMyCode

**Website**: [Available here](http://www.runmycode.org/).

**Description**: RunMyCode enables scientists to openly share the code and data that underlie their research publications. This service is based on the innovative concept of a companion website associated with a scientific publication.

**Thoughts**:

* It simply gives you hosting space for a simple website and a tarball of your dataset.
* Doesn't, as the website implies, run any code.
* Has some mechanisms for tagging and linking related projects together.
* Has some statistics gathering for views/downloads, etc.

### Internet Archive Software Library

**Website**: [Available here](https://archive.org/details/softwarelibrary_msdos_games).

**Description**: The Internet Archive Software Library is the ultimate software crate-digger's dream: Tens of thousands of playable software titles from multiple computer platforms, allowing instant access to decades of computer history in your browser through the JSMESS emulator.

The intention is to ultimately have most major computer platforms available; currently, the collection includes the Apple II, Atari 800, and ZX Spectrum computers. In each case, sub-collections contain vast sets of disk and cartridge images. Genres include games, applications, utilities, demos and operating systems.

To see a curated set of these many programs, with contextual descriptions and information, visit the Historical Software Collection, curated by the Internet Archive's Jason Scott. If you'd like to volunteer to help describe and increase knowledge on disks in the Software Library, contact Jason at jscott@archive.org.

**Thoughts**:

* Privately curated.
* Allows one to run software inside the browser by using javascript-transpiled emulators.
* Static archival: modifications and 'saved' information are essentially lost when the browser is closed.
* It would be interesting and possible for OCCAM to have such a 'widget' for computational objects.

## Remote Simulation

### nanoHUB / hubzero

**Website**: [Available here](https://nanohub.org/) using [hubzero](https://hubzero.org/).

**Description**: HUBzero® is a powerful, open source software platform for creating dynamic web sites that support scientific research and educational activities.

Modeling and simulation are arguably underutilized in both research and teaching. On nanoHUB.org, you can run over 320 simulation tools in a web browser. Simulation tools appear to run as applets in the browser window, but they are actually powered by a sophisticated cyberinfrastructure and run transparently in a scientific computing cloud that taps into Purdue University and national grid resources. nanoHUB uptime regularly exceeds 99%.

Anyone with a nanoHUB username and password can run simulations on nanoHUB. If you do not have a nanoHUB account, you can register online. Registration is quick, easy, and completely free! We use the registration information you provide to report quarterly usage statistics to the National Science Foundation. Installation requirements are minimal: You need to enable Javascript and cookies and have at least Java 1.4 installed.

nanoHUB is operated by the Network for Computational Nanotechnology (NCN), whose mission is to enable the use of modeling and simulation in the advancement of nanoscience and nanotechnology. Most of the tools on nanoHUB are derived from NCN’s contributions to research in the nano areas of electronics, mechanics, bio, photonics, and materials, primarily through leveraged funding.

**Thoughts**:

* Free: 1 GB storage, 3 concurrent simulation sessions
* Paid: 100 GB, 10 concurrent simulation sessions, $6.25/month, $75/year
* Also (through hubzero) hosts landing pages for papers/tools.

## Workflow Management

### VisTrails

**Website**: [Available here](http://www.vistrails.org/index.php/Main_Page). For [download here](http://www.vistrails.org/index.php/Downloads).

**Description**: VisTrails is an open-source scientific workflow and provenance management system that provides support for simulations, data exploration and visualization. Whereas workflows have been traditionally used to automate repetitive tasks, for applications that are exploratory in nature, such as simulations, data analysis and visualization, very little is repeated---change is the norm. As an engineer or scientist generates and evaluates hypotheses about data under study, a series of different, albeit related, workflows are created while a workflow is adjusted in an interactive process. VisTrails was designed to manage these rapidly-evolving workflows. 

A key distinguishing feature of VisTrails is a comprehensive provenance infrastructure that maintains detailed history information about the steps followed and data derived in the course of an exploratory task: VisTrails maintains provenance of data products, of the workflows that derive these products and their executions. This information is persisted as XML files or in a relational database, and it allows users to navigate workflow versions in an intuitive way, to undo changes but not lose any results, to visually compare different workflows and their results, and to examine the actions that led to a result. It also enables a series operations and user interfaces that simplify workflow design and use, including the ability to create and refine workflows by analogy and to query workflows by example.

VisTrails supports the creation and execution of workflows. It allows the combination of loosely-coupled resources, specialized libraries, grid and Web services. The released version comes with support for several packages including, VTK, Image Magick, Web Services, and pylab. You can also download packages contributed by our users, as well as create your own packages/modules. There are also a number of projects extending or building on top of VisTrails. Workflows can be run interactively, through the VisTrails GUI, or in batch using a VisTrails server.

VisTrails 1.0 was released in October 2007. Since then, the system has been downloaded thousands of times. VisTrails is written in Python and it uses the multi-platform Qt library for its user interface. It runs on Mac, Linux and Windows.

**Thoughts**:

* Local service. Runs in a GUI.
* Thus, requires the installation of a local program. Archival of resources to execute this program may not be available.
* Proprietary formats instead of common json.
* Provenance is good. Not sure what types of history it produces.
* Not necessarily easy/good for review sharing and quick exploration.

### Pegasus

**Website**: [Available here](http://pegasus.isi.edu/).

**Description**: The Pegasus project encompasses a set of technologies that help workflow-based applications execute in a number of different environments including desktops, campus clusters, grids, and clouds. Pegasus bridges the scientific domain and the execution environment by automatically mapping high-level workflow descriptions onto distributed resources. It automatically locates the necessary input data and computational resources necessary for workflow execution.Pegasus enables scientists to construct workflows in abstract terms without worrying about the details of the underlying execution environment or the particulars of the low-level specifications required by the middleware (Condor, Globus, or Amazon EC2). Pegasus also bridges the current cyberinfrastructure by effectively coordinating multiple distributed resources.

**Thoughts**:

* Makes use of HTCondor.
* Provenance is collected and maintained through Kickstart configurations.
* Portability (though not repeatability) allows workflows to be attempted to run in various environments by simply explicitly supporting these environments.
* Essentially lets you script commands and their dependencies to deploy on Condor.
* Similar to Swift in many ways.
* We already do this but instead of commands, we manage objects and their execution via docker/kvm.

### Galaxy

**Website**: [Available here](https://galaxyproject.org/).

**Desription**: Galaxy is an open, web-based platform for data intensive biomedical research. Whether on the free public server or your own instance, you can perform, reproduce, and share complete analyses. **Data intensive biology for everyone.**

**Thoughts**:

* Gives you access to various open data related to bio research.
* Workflows allow you to do basic operations on this data in order to filter or search or explore the substantially large, for instance, genetic data set.
* Can run your own instance!! No interoperability besides site-to-site import via url.
* Repeatability seems to be reliant on the availability of the data sets, which are external.
* Can spawn on a personal AWS instance.

### Taverna 2

**Website**: [Available here](http://www.taverna.org.uk/).

**Description**: Taverna is an open source and domain-independent Workflow Management System – a suite of tools used to design and execute scientific workflows and aid in silico experimentation.

Taverna has been created by the myGrid team and is currently funded though FP7 projects BioVeL, SCAPE and Wf4Ever.

The Taverna tools include the Workbench (desktop client application), the Command Line Tool (for a quick execution of workflows from a terminal), the Server (for remote execution of workflows) and the Player (Web interface plugin for submitting workflows for remote execution). Taverna Online lets you create Taverna workflows from a Web browser.

### Swift

**Website**: [Available here](http://swift-lang.org/main/).

**Description**:  Swift lets you write parallel scripts that run many copies of ordinary programs concurrently, using statements like this:

{% highlight javascript %}
foreach protein in proteinList {
  runBLAST(protein);
}
{% endhighlight %}

Swift is parallel: it runs multiple programs concurrently as soon as their inputs are available, reducing the need for complex parallel programming.

Swift is easy: Short, simple scripts can do large-scale work. The same script runs on multicore computers, clusters, grids, clouds, and supercomputers.

Swift is fast: it can run a million programs, thousands at a time, launching hundreds per second.

Swift is flexible: its being used in many fields of science, engineering, and business. Read the case studies.

**Thoughts**:

* Swift is a scripting language where it automatically parallelizes simple workloads.
* It can parallelize a foreach loop that creates a loop and know that passing that array implies a job dependency.
* OCCAM knows this through typed-object workflows, and can already do things quite well on its own.

## Job Scheduling

### DAGman

**Website**: [Available here](http://research.cs.wisc.edu/htcondor/dagman/dagman.html).

**Description**: DAGMan (Directed Acyclic Graph Manager) is a meta-scheduler for HTCondor. It manages dependencies between jobs at a higher level than the HTCondor Scheduler.

A directed acyclic graph (DAG) can be used to represent a set of programs where the input, output, or execution of one or more programs is dependent on one or more other programs. The programs are nodes (vertices) in the graph, and the edges (arcs) identify the dependencies. HTCondor finds machines for the execution of programs, but it does not schedule programs (jobs) based on dependencies. The Directed Acyclic Graph Manager (DAGMan) is a meta-scheduler for HTCondor jobs. DAGMan submits jobs to HTCondor in an order represented by a DAG and processes the results. An input file defined prior to submission describes the DAG, and a HTCondor submit description file for each program in the DAG is used by HTCondor.

Each node (program) in the DAG needs its own HTCondor submit description file. As DAGMan submits jobs to HTCondor, it uses a single HTCondor log file to enforce the ordering required for the DAG. The DAG itself is defined by the contents of a DAGMan input file. DAGMan is responsible for scheduling, recovery, and reporting for the set of programs submitted to HTCondor.

**Thoughts**:

* Built on HTCondor.
* Represents work as a DAG (simple parent-child dependencies)
* OCCAM jobs occasionally require co-running tasks, which are not representable by Condor. You could simulate with a single Condor job that spawns multiple OCCAM tasks.
* Condor's resume/halt/management could be leveraged by OCCAM. (The dispatcher could simply dispatch Condor jobs with notifications to retain/store resulting objects.)
* OCCAM could provide a job backend plugin infrastructure, which we could provide a simple, base solution for. Condor could be implemented as a plugin on top.

### HTCondor

**Website**: [Available here](http://research.cs.wisc.edu/htcondor/).

**Description**: Our goal is to develop, implement, deploy, and evaluate mechanisms and policies that support High Throughput Computing (HTC) on large collections of distributively owned computing resources. Guided by both the technological and sociological challenges of such a computing environment, the Center for High Throughput Computing at UW-Madison has been building the open source HTCondor distributed computing software (pronounced "aitch-tee-condor") and related technologies to enable scientists and engineers to increase their computing throughput.

**Thoughts**:

* OCCAM jobs occasionally require co-running tasks, which are not representable by Condor. You could simulate with a single Condor job that spawns multiple OCCAM tasks.
* Condor's resume/halt/management could be leveraged by OCCAM. (The dispatcher could simply dispatch Condor jobs with notifications to retain/store resulting objects.)
* OCCAM could provide a job backend plugin infrastructure, which we could provide a simple, base solution for. Condor could be implemented as a plugin on top.

### Portable Batch System (PBS)

**Website**: [Available here](http://www.pbsworks.com/).

**Description**: PBS Professional® is Altair's industry-leading workload manager and job scheduler for high-performance computing (HPC) environments.

Proven for over 20 years at thousands of companies worldwide, PBS Professional® was designed to improve productivity, optimize resource utilization & efficiency, and simplify the process of workload management for HPC clusters, clouds and supercomputers. PBS Professional’s powerful job scheduler automates workload scheduling, managing, monitoring, and reporting, and is the trusted solution for complex Top500 systems as well as smaller cluster owners.

**Thoughts**:

* For HPC environments.
* Commercial: $17.25 USD per cpu core.
* Many applications (open-source and commercial) are integrated to deploy to PBS.
