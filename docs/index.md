---
layout: doc_page
---

# OCCAM System Documentation

Welcome to OCCAM.
Here you will find instructions and documentation on how to integrate OCCAM into your research.
If you are a simulator developer, look at our [Getting Started](getting_started.html) page.
On the left, you will find navigation for specific parts of the OCCAM specification.

## Related Work

We have a [related work section](related_work.html) devoted to a comprehensive list and overview of related projects.

## Introduction to the System Architecture

OCCAM is a complex system with several major components.

* [Configuration](architecture/config.html): How OCCAM is configured at the low level. A description of `config.yml`.
* [The Worker Queue](architecture/worker_queue.html): A description of how OCCAM turns an experiment workflow into a set of tasks, dispatches them, and ultimately creates/stores new data.
* [The Object Store](architecture/object_store.html): How objects are stored, replicated, and distributed within the system. How we mitigate repeatability issues of availability.
* [Object Configurator](architecture/configurator.html): How objects specify their possible configurations and how forms are generated around the input schema. How can you provide your own configuration widget.
* [The Workflow Editor](architecture/workflow_editor.html): How objects are used in the workflow editor. How inputs/outputs are described in metadata and used by the workflow editor and how they are stored in the experiment.
* [Visualizers and Plotters](architecture/visualizers.html): How the graph builder uses the output schema and output data to feed graph data. How can you build your own graph type.
* Federation and You: How OCCAM replicates data and information across several independent nodes. (Coming Soon)

## Introduction to the Command Line Toolkit

The OCCAM system can be invoked through a command line tool.

* [Commands](commands.html): The various OCCAM commands and their usage.

## Introduction for Simulator Developers

OCCAM provides a sophisticated and standard interface for simulator interaction.
It does not just queue and manage running simulations, but also provides a means of configuring
and validating user input, and generating sets of experiments automatically.
OCCAM can also automatically generate graphs and visualizations from the resulting data.

To do this, OCCAM needs to know what data the simulator expects and generates and the form of that data.
Each simulator is described separately with a set of meta-data.

* `object.json`: An [Object Description](object.html) describes the basic meta-data for an object (simulator, benchmark, etc), such as name, authors, and a summary of what it does.
* `input_schema.json`: The [Input Schema](input_schema.html) specifies the configuration input.
* `output_schema.json`: The [Output Schema](output_schema.html) handles the representation of any possible resulting data.

The OCCAM system will then generate for each experiment an input file based upon the user input called [`input.json`](input).
The object/simulator may then read that input file natively, or must provide an auxiliary script to create the native configuration files it needs.

Generally, the object/simulator must provide a run script that will run an experiment using the `input.json`. Our dispatcher will run that script.

An object/simulator must also provide a complete automated script to build the object.

To see a working example of the files that provide a simulator in OCCAM, we have code repositories for several simulators:

* [DRAMSim2](https://bitbucket.org/occam/occam-simulator-dramsim2/src)
* [Sniper](https://bitbucket.org/occam/occam-simulator-sniper/src)
* [Manifold](https://bitbucket.org/occam/occam-simulator-manifold/src)
